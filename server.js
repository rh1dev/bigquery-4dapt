const express = require('express')
const BigQuery = require('@google-cloud/bigquery')
const app = express()// Instantiates a client

const bigquery = BigQuery({
  projectId: 'fit-coral-171917'
})

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.listen(5004, function () {
  console.log('Example app listening on port 3000!')
})
